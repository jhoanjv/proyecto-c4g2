package co.com.jhoan.ventasdomiciliog2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;

    private AppCompatButton btnLogin;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnGoogle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);
        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> onFacebookClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> onGoogleClick());
    }


    private void onLoginClick(){

        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        Intent intent = new Intent(LoginActivity.this, PaymentActivity.class);
        startActivity(intent);


    }

    private void onFacebookClick(){
        Intent intent = new Intent(LoginActivity.this, PaymentActivity.class);
        startActivity(intent);
    }

    private void onGoogleClick(){
        Intent intent = new Intent(LoginActivity.this, PaymentActivity.class);
        startActivity(intent);
    }


}