package co.com.jhoan.ventasdomiciliog2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;


import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;

public class PaymentActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        initUI();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        toolbar = findViewById(R.id.app_tooolbar);
        toolbar.setNavigationOnClickListener(v ->drawerLayout.openDrawer(navigationView));

        navigationView = findViewById(R.id.nv_payments);
        navigationView.setNavigationItemSelectedListener(menuItem ->{
            menuItem.setChecked (true);
            drawerLayout.closeDrawers();
            return true;
        });
    }

    private boolean onMenuItemClick(MenuItem menuItem){
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }
}